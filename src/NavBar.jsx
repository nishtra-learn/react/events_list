import React from 'react';
import { NavLink } from 'react-router-dom';
import * as eventCategories from './eventCategories';

export default function NavBar() {
    return (
        <nav className="navbar navbar-light bg-light">
            <span className="navbar-brand mb-0 h1">Planned Events</span>
            <div className="mr-auto d-flex">
                <NavLink exact to="/" className="navLink">Home</NavLink>
                <NavLink to={`/${eventCategories.music}`} className="navLink">Music</NavLink>
                <NavLink to={`/${eventCategories.cinema}`} className="navLink">Cinema</NavLink>
                <NavLink to={`/${eventCategories.community}`} className="navLink">Community</NavLink>
            </div>
        </nav>
    );
}