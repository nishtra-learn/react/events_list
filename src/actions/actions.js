import * as actionTypes from './actionTypes';

export const getEvents = () => ({
    type: actionTypes.GET_EVENTS
})

export const searchEventById = () => ({
    type: actionTypes.GET_EVENT_BY_ID,
})

export const getEventByIdSuccess = (event) => ({
    type: actionTypes.GET_EVENT_BY_ID_SUCCESS,
    payload: event
})

export const getEventByIdFail = () => ({
    type: actionTypes.GET_EVENT_BY_ID_FAIL,
})

export function getEventById(id) {
    return (dispatch, getState) => {
        dispatch(searchEventById())

        let event = getState().events.collection.find(item => item.id == id);
        
        if (event) {
            dispatch(getEventByIdSuccess(event))
        }
        else {
            dispatch(getEventByIdFail());
        }
    }
}

export const createEvent = (event) => ({
    type: actionTypes.CREATE_EVENT,
    payload: event
})

export const showEventFormModal = () => ({
    type: actionTypes.SHOW_EVENT_FORM_MODAL
})

export const hideEventFormModal = () => ({
    type: actionTypes.HIDE_EVENT_FORM_MODAL
})