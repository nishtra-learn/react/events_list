import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { getEventById } from "./actions/actions";

class EventPage extends React.Component {

    constructor(props) {
        super(props);

        this.eventId = this.props.match.params.id;
        props.dispatch(getEventById(this.eventId));
    }

    render() {
        let { event: foundEvent, searching, searchFailed } = this.props;
        let event = {
            title: "title",
            category: "category",
            description: "description"
        }

        const renderContent = () => {
            if (searching)
                return <p>Searching...</p>
            if (searchFailed)
                return <p>Unable to find event with id {this.eventId}.</p>
            if (foundEvent) {
                event = foundEvent;
                return (
                    <>
                        <h2 className="mb-0">{event.title}</h2>
                        <div className="text-muted font-italic">{event.category}</div>
                        <p className="mt-2">{event.description}</p>
                    </>
                )
            }
        }

        return (
            <>
                {renderContent()}
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    events: state.events.collection,
    event: state.events.selectedEvent,
    searching: state.events.searchingEvent,
    searchFailed: state.events.searchByIdFailed
})

export default connect(mapStateToProps)(EventPage)