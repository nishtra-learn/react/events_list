import React from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getEventById } from "./actions/actions";

export default function EventItem(props) {
    let event = props.event;
    return (
        <>
            <article key={event.id} className="eventItem">
                <Link to={`/${event.id}`}>
                    <h4>{event.title}</h4>
                    <p>{event.description.substring(0, 100)}</p>
                </Link>
            </article>
            <hr />
        </>
    )
}