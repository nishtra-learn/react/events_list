import logo from './logo.svg';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Redirect, Route, Switch } from 'react-router-dom';
import EventsList from './EventsList';
import * as eventCategories from './eventCategories';
import NavBar from './NavBar';
import EventPage from './EventPage';
import { connect } from 'react-redux';
import { getEvents } from './actions/actions';
import { useEffect } from 'react';

function App({events, getEvents, selectedEvent }) {
  useEffect(() => {
    if (events.length === 0){
      getEvents()
    }
  }, [events, getEvents])

  return (
    <>
      <NavBar></NavBar>
      <div className="container">
        <Switch>
          <Route exact path="/">
            <EventsList event-category={eventCategories.all}></EventsList>
          </Route>
          <Route exact path={`/${eventCategories.music}`}>
            <EventsList event-category={eventCategories.music}></EventsList>
          </Route>
          <Route exact path={`/${eventCategories.cinema}`}>
            <EventsList event-category={eventCategories.cinema}></EventsList>
          </Route>
          <Route exact path={`/${eventCategories.community}`}>
            <EventsList event-category={eventCategories.community}></EventsList>
          </Route>
          <Route path="/:id(\d+)" component={EventPage}></Route>
          <Redirect to="/"></Redirect>
        </Switch>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  events: state.events.collection,
  selectedEvent: state.events.selectedEvent
})

const mapDispatchToProps = {
  getEvents
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
