import { combineReducers } from 'redux';
import eventsReducer from './eventsReducer';
import modalReducer from "./modalReducer";

const rootReducer = combineReducers({
    events: eventsReducer,
    modals: modalReducer
})

export default rootReducer