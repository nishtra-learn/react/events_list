import * as actionTypes from '../actions/actionTypes';

export const initialState = {
    eventFormModalShow: false
}

export default function modalReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SHOW_EVENT_FORM_MODAL:
            return { ...state, eventFormModalShow: true };
        case actionTypes.HIDE_EVENT_FORM_MODAL:
            return { ...state, eventFormModalShow: false };
        default:
            return state
    }
}