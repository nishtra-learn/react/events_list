import React from 'react';
import { connect } from 'react-redux';
import EventItem from './EventItem';
import { showEventFormModal } from './actions/actions';
import EventFormModal from './EventFormModal';

function EventsList(props) {
    const showEventFormModal = props.showEventFormModal;
    let eventCategory = props['event-category'] ?? 'all';
    let events = props.events;
    let filteredEvents = events.filter((item) => eventCategory === 'all' || item.category === eventCategory);
    
    return (
        <>
            <div className="EventsList">
                <h2>{eventCategory}</h2>
                <button className="btn btn-primary mb-3" onClick={() => showEventFormModal()}>Add new event</button>
                {
                    filteredEvents.map(event => <EventItem key={event.id} event={event}></EventItem>)
                }
            </div>
            <EventFormModal></EventFormModal>
        </>
    );
}

const mapStateToProps = (state) => ({
    events: state.events.collection
})

const mapDispatchToProps = {
    showEventFormModal
}

export default connect(mapStateToProps, mapDispatchToProps)(EventsList)