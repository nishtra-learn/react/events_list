import * as actionTypes from '../actions/actionTypes';
import data from '../db.json';

export const initialState = {
    collection: [],
    nextId: 1,
    searchingEvent: false,
    searchByIdFailed: false,
    selectedEvent: null
}

export default function postsReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.GET_EVENTS: {
            let maxId = data.events.reduce(
                (acc, val) => val.id > acc ? val.id : acc
                , 1);
            let newState = { ...state, collection: data.events, nextId: maxId + 1 };
            return newState;
        }
        case actionTypes.GET_EVENT_BY_ID: {
            let newState = { ...state, searchingEvent: true };
            return newState;
        }
        case actionTypes.GET_EVENT_BY_ID_SUCCESS: {
            let event = action.payload;
            return { ...state, selectedEvent: event, searchingEvent: false, searchByIdFailed: false };
        }
        case actionTypes.GET_EVENT_BY_ID_FAIL: {
            return { ...state, selectedEvent: null, searchingEvent: false, searchByIdFailed: true };
        }
        case actionTypes.CREATE_EVENT: {
            let newEvent = { id: state.nextId, ...action.payload };
            let events = [...state.collection, newEvent];
            return { ...state, collection: events, nextId: state.nextId + 1 };
        }
        default:
            return state
    }
}