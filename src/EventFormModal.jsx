import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { connect } from 'react-redux';
import { showEventFormModal, hideEventFormModal, createEvent } from "./actions/actions";

function EventFormModal(props) {
    const showModal = props.showModal;
    const hideModal = props.hideModal;
    const createEvent = props.createEvent;
    let show = props.show;
    let modalTitle = "New event";

    const [title, setTitle] = useState('');
    const [category, setCategory] = useState('music');
    const [description, setDescription] = useState('');
    const resetInputFields = () => {
        setTitle('');
        setCategory('music');
        setDescription('');
    }

    const handleClose = () => hideModal();
    const handleShow = () => showModal();
    const handleSaveBtnClick = () => {
        let newEvent = {
            category,
            title,
            description
        }
        createEvent(newEvent);
        resetInputFields();
        handleClose();
    }
    const handleModalFieldsChange = (e) => {
        let name = e.target.name;
        let val = e.target.value;
        switch (name) {
            case 'title':
                setTitle(val)
                break;
            case 'category':
                setCategory(val)
                break;
            case 'description':
                setDescription(val)
                break;
            default:
                break;
        }
    }

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            size="lg"
            centered
            className="EventFormModal"
        >
            <Modal.Header closeButton>
                <Modal.Title>{modalTitle}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="container">
                    <div className="form-group">
                        <label>Title</label>
                        <input type="text" className="form-control" name="title" value={title} onChange={handleModalFieldsChange} required/>
                    </div>
                    <div className="form-group">
                        <label>Category</label>
                        <select name="category" className="custom-select" value={category} onChange={handleModalFieldsChange} required>
                            <option value="music">Music</option>
                            <option value="cinema">Cinema</option>
                            <option value="community">Community</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label >Details</label>
                        <textarea name="description" className="form-control" rows="4" value={description} onChange={handleModalFieldsChange}></textarea>
                    </div>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <button className="btn btn-primary" onClick={handleSaveBtnClick}>Save</button>
                <button className="btn btn-secondary" onClick={handleClose}>Close</button>
            </Modal.Footer>
        </Modal>
    );
}

const mapStateToProps = (state) => ({
    show: state.modals.eventFormModalShow
})

const mapDispatchToProps = {
    showModal: showEventFormModal,
    hideModal: hideEventFormModal,
    createEvent
}

export default connect(mapStateToProps, mapDispatchToProps)(EventFormModal)